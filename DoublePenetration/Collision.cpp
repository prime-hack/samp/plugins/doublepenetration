#include "Collision.h"
#include <CGame/CPed.h>
#include <CGame/CVehicle.h>

extern "C" bool addHelper( HelperInfo &helper );
extern "C" bool statusHelper( std::string_view helper );
extern "C" bool toggleHelper( std::string_view helper, const bool &status );

Collision::Collision( SRDescent *parent )
	: SRDescent( parent ), helper( &status, { VK_LCONTROL, VK_LSHIFT, 'C' }, "collision" ) {
	antiCollision.install();
	antiCollision.onBefore += [this]( SRHook::Info &info ) {
		if ( !status ) return;
		if ( !LOCAL_PLAYER ) return;
		if ( !LOCAL_PLAYER->isValid() ) return;
		if ( LOCAL_PLAYER->isActorDead() ) return;
		auto entity1 = (CEntity *)info.cpu.ESI;
		auto entity2 = (CEntity *)info.cpu.EDI;
		if ( !entity1 || !entity2 ) {
			info.retAddr	  = 0x54CF8D;
			info.skipOriginal = true;
			return;
		}
		if ( entity1->nType > 1 && entity2->nType > 1 &&
			 ( ( LOCAL_PLAYER == entity1 || LOCAL_PLAYER == entity2 ) ||
			   ( LOCAL_PLAYER->isDriving() &&
				 ( LOCAL_PLAYER->vehicle == entity1 || LOCAL_PLAYER->vehicle == entity2 ) ) ) ) {
			info.retAddr	  = 0x54CF8D;
			info.skipOriginal = true;
			return;
		} else if ( ( entity1->nType == 1 || entity2->nType == 1 ) &&
					( ( LOCAL_PLAYER == entity1 || LOCAL_PLAYER == entity2 ) ||
					  ( LOCAL_PLAYER->isDriving() &&
						( LOCAL_PLAYER->vehicle == entity1 || LOCAL_PLAYER->vehicle == entity2 ) ) ) ) {
			info.retAddr	  = 0x54CEFC;
			info.skipOriginal = true;
			return;
		}
	};
	helper.statusChanged = [this]( bool status ) {
		if ( status ) {
			camCars.enable();
			camPeds.enable();
			camBuilds.enable();
			camObjs.enable();
			camCache.enable();
		} else {
			camCars.disable();
			camPeds.disable();
			camBuilds.disable();
			camObjs.disable();
			camCache.disable();
		}
	};
	helper.mainloop = [this] {
		if ( !status ) return;
		if ( !LOCAL_PLAYER ) return;
		if ( !LOCAL_PLAYER->isValid() ) return;
		if ( LOCAL_PLAYER->isActorDead() ) return;
		CPhysical *phys = LOCAL_PLAYER;
		if ( LOCAL_PLAYER->isDriving() ) phys = LOCAL_PLAYER->vehicle;

		uint8_t withGround;
		float	delta = phys->matrix->pos.fZ - CallFunc::ccall<float>( 0x5696C0, phys->matrix->pos, &withGround, 0 );
		CallFunc::ccall<float>( 0x5696C0, phys->matrix->pos - RwV3D( 0, 0, delta - 0.1f ), &withGround, 0 );
		if ( !withGround ) {
			if ( !statusHelper( "AirBreak" ) ) {
				ab_status = statusHelper( "AirBreak" );
				toggleHelper( "AirBreak", true );
			}
		} else if ( ab_status != 2 && delta > 1.0f ) {
			toggleHelper( "AirBreak", ab_status );
			ab_status = 2;
		}
	};
	addHelper( helper );
}

Collision::~Collision() {
	antiCollision.remove();
}
